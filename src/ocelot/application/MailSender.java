package ocelot.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServlet;

public class MailSender {

    private Properties properties;

    private MailSender(Properties props) {
        this.properties = props;
    }

    public static MailSender create(Properties properties) {
        if (properties == null) {
            return null;
        }
        return new MailSender(properties);
    }

    public void sendMail(String to, String subject, String content) {

        Session session = Session.getDefaultInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(properties.getProperty("username"),properties.getProperty("passwd"));
                    }
                });

        try {



            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(properties.getProperty("username")));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject(subject);

            //message.setText(content);

            //message.setContent("<h2>Hello</h2><a href=\"google.com\">Google</a>", "text/html; charset=utf-8");
            message.setContent(content, "text/html; charset=utf-8");

            //message.setContent(content, "text/html; charset=utf-8");

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }


    }

    public static void main(String[] args) {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("username", "ocelot.pgc@gmail.com");
        props.put("passwd", "iamacatmeow");
        props.put("mail.debug", true);

        MailSender sender = MailSender.create(props);


        sender.sendMail("tangyiyi008@gmail.com", "Ocelot", "My test mail from PGC");



    }
}
