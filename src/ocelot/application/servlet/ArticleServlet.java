package ocelot.application.servlet;

import ocelot.application.beans.Article;
import ocelot.application.DBConnection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;

public class ArticleServlet extends JsonServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        JSONObject jobj = (JSONObject) JSONValue.parse(req.getReader());

        if (jobj == null ) {
            resp.setStatus(400);
            return;
        }

        String method = jobj.get("method").toString();

        switch (method) {
            case "getLastestArticles": {
                int limit = Integer.valueOf(jobj.get("limit").toString());
                int offset = Integer.valueOf(jobj.get("offset").toString());
                boolean filterBanned = (Boolean) jobj.get("filter_banned");
                Long unix_time = (Long) jobj.get("currentTime");
                Timestamp currentTime = null;
                if (unix_time != null) {
                    currentTime = new Timestamp(unix_time);
                }

                doGetLastestArticles(req, resp, limit, offset, filterBanned, currentTime);
                return;
            }

            case "getArticleById": {

                String id = (String) jobj.get("id");

                doGetArticleById(req, resp, id);
                return;
            }

            case "getLastestArticlesOfUser": {
                String userId = (String) jobj.get("user_id");
                int limit = Integer.valueOf(jobj.get("limit").toString());
                int offset = Integer.valueOf(jobj.get("offset").toString());
                boolean filterBanned = (Boolean) jobj.get("filter_banned");

                doGetLastestArticlesOfUser(req, resp, userId, limit, offset, filterBanned);
                return;
            }

            case "addArticle": {
                Article article = new Article();
                article.setUserId(jobj.get("user_id").toString());
                article.setContent(jobj.get("content").toString());
                article.setTitle(jobj.get("title").toString());
                article.setPubtime(new Timestamp((long)jobj.get("pubtime")));
                doAddAriticle(req, resp, article);
                return;
            }

            case "deleteArticleById": {
                String id = (String) jobj.get("id");
                doDeleteArticleById(req, resp, id);
                return;
            }

            case "modifyArticleById": {
                String id = (String) jobj.get("id");
                String title = (String) jobj.get("title");
                String content = (String) jobj.get("content");

                doModifyArticleById(req, resp, id, title, content);
                return;
            }

            case "setArticleBanned": {
                String id = (String) jobj.get("id");
                boolean banned = (boolean) jobj.get("banned");

                doSetArticleBanned(req, resp, id, banned);
                return;
            }

            case "searchArticles": {
                int limit = Integer.valueOf(jobj.get("limit").toString());
                int offset = Integer.valueOf(jobj.get("offset").toString());
                JSONArray rules = (JSONArray) jobj.get("rules");
                Timestamp currentTime = new Timestamp((Long) jobj.get("currentTime"));
                doSearchArticles(req, resp, rules, currentTime, limit, offset);
                return;
            }


            default:
                sendError(req, resp, 400,  "No such method " + method + "()");
                break;
        }
    }

    private void doGetLastestArticles(HttpServletRequest req, HttpServletResponse resp,
                                      int limit, int offset, boolean filterBanned, Timestamp currentTime) throws ServletException, IOException {

        try (DBConnection connection = createDBConnection()){

            List<Article> list = connection.getLastestArticles(limit, offset, filterBanned, currentTime);

            JSONObject jobj = new JSONObject();
            JSONArray articleArray = new JSONArray();


            for (Article article : list) {
                JSONObject obj = article.toJson();
                System.out.println("article :" + article.getTitle() + ", pubtime:" + article.getPubtime() + "(" + article.getPubtime().getTime() + ")");
                articleArray.add(obj);
            }

            jobj.put("result", articleArray);

            resp.setStatus(200);
            resp.setContentType("application/json");
            resp.getWriter().println(jobj.toJSONString());

        } catch (SQLException e) {
            sendError(req, resp, 400, e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
            e.printStackTrace();
        }
    }


    private void doGetArticleById(HttpServletRequest req, HttpServletResponse resp,
                                      String id) throws ServletException, IOException {

        try (DBConnection connection = createDBConnection()){

            Article article = connection.getArticleById(id);

            JSONObject jobj = new JSONObject();

            jobj.put("result", article.toJson());

            resp.setStatus(200);
            resp.setContentType("application/json");
            resp.getWriter().println(jobj.toJSONString());

        } catch (SQLException e) {
            sendError(req, resp, 400, e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
            e.printStackTrace();
        }
    }

    private void doGetLastestArticlesOfUser(HttpServletRequest req, HttpServletResponse resp,
                                      String userId, int limit, int offset, boolean filterBanned) throws ServletException, IOException {

        try (DBConnection connection = createDBConnection()){

            List<Article> list = connection.getLastestArticlesOfUser(userId, limit, offset, filterBanned);

            JSONObject jobj = new JSONObject();
            JSONArray articleArray = new JSONArray();

            for (Article article : list) {
                JSONObject obj = article.toJson();
                System.out.println("article :" + article.getTitle() + ", pubtime:" + article.getPubtime() + "(" + article.getPubtime().getTime() + ")");
                articleArray.add(obj);
            }

            jobj.put("result", articleArray);

            resp.setStatus(200);
            resp.setContentType("application/json");
            resp.getWriter().println(jobj.toJSONString());

        } catch (SQLException e) {
            sendError(req, resp, 400, e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
        }
    }

    private void doAddAriticle(HttpServletRequest req, HttpServletResponse resp,
                               Article article) throws ServletException, IOException {

        String err = "";

        try(DBConnection connection = createDBConnection()) {
            connection.addArticle(article);

            resp.setStatus(200);
            resp.setContentType("application/json");

            JSONObject jobj = new JSONObject();
            jobj.put("error", null);

            resp.getWriter().println(jobj.toJSONString());
        } catch (SQLException e) {
            //e.printStackTrace();
            sendError(req, resp,400, e.getMessage());


        } catch (Exception e) {
            //e.printStackTrace();
            sendError(req, resp, 400, e.getMessage());

        }

    }

    private void doDeleteArticleById(HttpServletRequest req, HttpServletResponse resp,
                                   String article_id) throws ServletException, IOException {

        try(DBConnection connection = createDBConnection()) {
            connection.deleteArticleById(article_id);

            resp.setStatus(200);
            resp.setContentType("application/json");

            JSONObject jobj = new JSONObject();
            jobj.put("error", null);

            resp.getWriter().println(jobj.toJSONString());
        } catch (SQLException e) {

            sendError(req, resp,400, e.getMessage());

        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
        }
    }


    private void doModifyArticleById(HttpServletRequest req, HttpServletResponse resp,
                                     String article_id, String title,String content) throws ServletException, IOException {

        try(DBConnection connection = createDBConnection()) {
            connection.modifyAricleContent(article_id, title, content);

            resp.setStatus(200);
            resp.setContentType("application/json");

            JSONObject jobj = new JSONObject();
            jobj.put("error", null);

            resp.getWriter().println(jobj.toJSONString());
        } catch (SQLException e) {

            sendError(req, resp,400, e.getMessage());

        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
        }
    }

    private void doSetArticleBanned(HttpServletRequest req, HttpServletResponse resp,
                                    String article_id, boolean banned) throws ServletException, IOException {

        try(DBConnection connection = createDBConnection()) {
            connection.setArticleBanned(article_id, banned);

            resp.setStatus(200);
            resp.setContentType("application/json");

            JSONObject jobj = new JSONObject();
            jobj.put("error", null);

            resp.getWriter().println(jobj.toJSONString());
        } catch (SQLException e) {

            sendError(req, resp,400, e.getMessage());

        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
        }
    }

    private void doSearchArticles(HttpServletRequest req, HttpServletResponse resp,
                                      JSONArray rules, Timestamp currentTime, int limit, int offset) throws ServletException, IOException {

        try (DBConnection connection = createDBConnection()){

            List<Article> list = connection.searchAndSortArticles(rules, currentTime, limit, offset);

            JSONObject jobj = new JSONObject();
            JSONArray articleArray = new JSONArray();

            for (Article article : list) {
                JSONObject obj = article.toJson();
                articleArray.add(obj);
            }

            jobj.put("result", articleArray);

            resp.setStatus(200);
            resp.setContentType("application/json");
            resp.getWriter().println(jobj.toJSONString());

        } catch (SQLException e) {
            sendError(req, resp, 400, e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            sendError(req, resp, 400, e.getMessage());
            e.printStackTrace();
        }
    }


}
