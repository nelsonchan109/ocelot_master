package ocelot.application.servlet;

import ocelot.application.DBConnection;
import ocelot.application.Reset;
import ocelot.application.ResetPwdSessionManager;
import ocelot.application.beans.User;


import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;


public class ChangePasswdServlet  extends CommonServlet {



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();

        String index = req.getParameter("index");
        if (index == null) {
            showError(req, resp, 400, "Invalid parameter");
            return;
        }

        Reset reset = ResetPwdSessionManager.instance().takeReset(index);
        if (reset == null) {
            showError(req, resp, 400, "Reset Session not found, maybe your link has expired!");
            return;
        }

        User user = null;

        try(DBConnection connection = createDBConnection()) {

            user = connection.getUserById(reset.user_id);

            session.setAttribute("userSession", user);
            req.getRequestDispatcher("ResetPassword.jsp").forward(req, resp);
        } catch (SQLException e) {
            showError(req, resp, 400, e.getMessage());
        } catch (Exception e) {
            showError(req, resp, 400, e.getMessage());
        }

    }

}
