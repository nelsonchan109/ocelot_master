package ocelot.application.servlet;

import ocelot.application.DBConnection;
import ocelot.application.beans.Article;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class ViewSingleArticleServlet extends CommonServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String article_id = req.getParameter("article_id");
        if (article_id != null) {
            try (DBConnection connection = createDBConnection()){

                Article article = connection.getArticleById(article_id);
                if (article == null) {
                    sendErrorPage(req, resp, 400, "Resource can't be found");
                    return;
                }
                req.setAttribute("article", article);

                req.getRequestDispatcher("ViewArticle.jsp").forward(req, resp);

            } catch (SQLException e) {
                sendErrorPage(req, resp, 400, e.getMessage());
                return;
            } catch (Exception e) {
                sendErrorPage(req, resp, 400, e.getMessage());
                return;
            }

        } else {
            sendErrorPage(req, resp, 400, "Resource can't be found");
            return;
        }

    }
}
