package ocelot.application;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class ResetPwdSessionManager implements ServletContextListener {

    private static ResetPwdSessionManager s_manager = null;

    private ScheduledExecutorService scheduler = null;
    private ResetPwdLinkKeeper keeper = null;

    public static ResetPwdSessionManager instance() {

        if (s_manager == null) {
            s_manager = new ResetPwdSessionManager();
            //s_manager.start();
        }

        return s_manager;
    }

    public void start() {
        scheduler.scheduleAtFixedRate(keeper, 0, 5, TimeUnit.SECONDS);
    }

    public void stop() {
        scheduler.shutdownNow();
    }

    private ResetPwdSessionManager() {
        scheduler = Executors.newSingleThreadScheduledExecutor();
        keeper = new ResetPwdLinkKeeper();
    }


    public void putReset(String index, Reset reset) {
        keeper.putReset(index, reset);
    }

    public Reset takeReset(String index) {
        return keeper.takeReset(index);
    }

    public boolean hasReset(String index) {
        return keeper.hasReset(index);
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        start();

    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        stop();
    }
}
