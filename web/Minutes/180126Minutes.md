Agenda
=======
1. To discuss about retrieving data from database and include the data in our main page and personal page
2. To discuss about linking function to button and navigation bars of main page and personal page.
3. To discuss about how to implement nested comment.

Minutes/Notes
=======
1.Successfully implement AjaxEcho to request and respond when logged in.
- Every group member involved in implementing AjaEcho to login form.

2.Successfully implement submit article function
- Every group member involved in implementing submit article function coding and design.

3.Successfully retrieve article from database for main page.
- Every group member involved in writing code for retrieving article from database for main page.

4.Successfully return to pop up sign in page after key in wrong username or passwords
- Every group member involved in the design and coding of the function.

Next Steps:
=======
1. To implement retrieve article from database for main page and personal page. 
2. To implement log out function for personal page.
