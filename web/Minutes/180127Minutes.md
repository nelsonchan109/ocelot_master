Agenda
=======
1. To retrieve article from database for main page and personal page. 
2. To implement log out function for personal page.

Minutes/Notes
=======
1.Successfully retrieve article and picture from database for main page
- Every group member involved in the process of retrieving article and picture from database for main page.

2.Successfully designed and completed admin page layout
-Every group member involved in the process of design and coding of admin page layout.

Next Steps:
=======
1. To retrieve article from database for personal page. 
2. To successfully implement log out function for personal page. 
