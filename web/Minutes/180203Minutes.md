Agenda
=======
1. Modify navigation bar to look consistent. 
2. Modify bug for summernote.

Minutes/Notes
=======
1.Successfully modify navigation bar so that it will have a consistent look
- Every group members involved in the modifying of navigation bar so that it will have a consistent look.

2.Successfully implement search function for main page
- Every group members involved in the implementation of search function for main page.

Next Steps:
=======
1. Modify bug for summernote
2. Implement load more article function for main page
